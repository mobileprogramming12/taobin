import 'dart:io';

void main() {
  print('Touch to start\n');

  List<Map<String, int>> itemsmenu = [
    {'Strawberry milk shake': 50, 'category': 1},
    {'Iced thai Tea': 40, 'category': 1},
    {'Iced brown Sugar Milk': 55, 'category': 1},
    {'Hot mocha': 40, 'category': 2},
    {'Iced cafe latte': 45, 'category': 2},
    {'Hot espresso': 50, 'category': 2},
    {'Hot americano': 35, 'category': 2},
    {'Ice cappuccino': 40, 'category': 2},
    {'Hot black tea': 40, 'category': 3},
    {'Iced thai milk tea': 45, 'category': 3},
    {'Hot taiwanese tea': 50, 'category': 3},
    {'Hot americano': 35, 'category': 3},
    {'Ice cappuccino': 40, 'category': 3},
    {'Hot Cocoa': 35, 'category': 4},
    {'Oreo Chocy': 45, 'category': 4},
    {'Hot milk': 30, 'category': 4},
    {'Ice caramel cocoa': 30, 'category': 4},
    {'Ice pink milk': 30, 'category': 4},
    {'Matcha protein shake': 45, 'category': 5},
    {'Chocolate protein shake': 50, 'category': 5},
    {'Strawberry protein shake': 55, 'category': 5},
    {'Caramel protein shake': 40, 'category': 5},
    {'Plain protein shake': 35, 'category': 5},
    {'Pepsi': 30, 'category': 6},
    {'Iced limenade soda': 35, 'category': 6},
    {'Iced Strawberry': 40, 'category': 6},
    {'Iced plum soda': 30, 'category': 6},
    {'Iced brueberry soda': 45, 'category': 6},
  ];

  List<Map<String, int>> itemwithcategory;

  List<String> items1 = [
    'No added sugar 0%',
    'Little sweet 25%',
    'Sweet fit 50%',
    'So sweet 75%',
    'The sweetest 100%'
  ];

  List<String> items0 = [
    'เมนูแนะนำ',
    "กาแฟ",
    "ชา",
    "นม,โกโก้ และคาราเมล",
    "โปรตีนเชค",
    "โซดา และอื่นๆ",
  ];

  List<String> cart = [];
  int sum = 0;

  int? menu, level, category;

  while (true) {
    for (int i = 0; i < items0.length; i++) {
      print("${i + 1} => ${items0[i]}");
    }

    print("\nเลือกประเภทเครื่องดื่มที่ต้องการ");

    while (true) {
      category = int.parse(stdin.readLineSync()!);
      if (category < 1 || category > items0.length) {
        print("ไม่มีประเภทนี้ในระบบ กรุณาใส่เลขใหม่อีกครั้ง");
      } else {
        itemwithcategory = itemsmenu
            .where((element) => element['category'] == category)
            .toList();
        if (itemwithcategory.isNotEmpty) {
          break;
        } else {
          print('ยังไม่มีสินค้าในประเภทนี้ กรุณาเลือกใหม่');
        }
      }
    }
    print("\n");
    for (int i = 0; i < itemwithcategory.length; i++) {
      print(
          "${i + 1} => ${itemwithcategory[i].keys.first} ราคา ${itemwithcategory[i].values.first} บาท");
    }

    print("\nเลือกเมนูที่ต้องการ");

    while (true) {
      menu = int.parse(stdin.readLineSync()!);
      if (menu < 1 || menu > itemsmenu.length) {
        print("ไม่มีเมนูนี้ในระบบ กรุณาใส่เลขใหม่อีกครั้ง");
      } else {
        break;
      }
    }
    print("\n");
    for (int i = 0; i < items1.length; i++) {
      print("${i + 1} => ${items1[i]}");
    }

    print("\nเลือกความหวานที่ต้องการ");
    while (true) {
      level = int.parse(stdin.readLineSync()!);
      if (level < 1 || level > items1.length) {
        print("ไม่มีระดับความหวานนี้ในระบบ กรุณาใส่เลขใหม่อีกครั้ง");
      } else {
        break;
      }
    }

    print("\nคุณต้องการเพิ่มสินค้าอีกรึไม่\n(yes/no)");
    String? add;
    while (true) {
      add = stdin.readLineSync();
      cart.add('${menu - 1},${level - 1},$category');
      if (add == 'no' || add == 'yes') {
        break;
      }
    }
    if (add == 'no') {
      break;
    }
  }

  print('\nรายการทั้งหมดของคุณ');
  for (int i = 0; i < cart.length; i++) {
    List<String> m = cart[i].split(',');

    itemwithcategory = itemsmenu
        .where((element) => element['category'] == int.parse(m[2]))
        .toList();

    sum += itemwithcategory[int.parse(m[0])].values.first;
    print(
        '${i + 1}. ${itemwithcategory[int.parse(m[0])].keys.first} ระดับความหวานที่ต้องการ ${items1[int.parse(m[1])]} ราคา ${itemwithcategory[int.parse(m[0])].values.first} บาท');
  }
  print('รวมทั้งหมด $sum บาท\n');

  List<String> items3 = [
    'จ่ายเงินสด',
    'จ่ายด้วย QR',
    'ช้อปปี้เพย์ (Shopee Pay)',
    'ใช้เต่าบินเครดิต',
    'ใช้/ดู คูปองสะสม'
  ];

  while (true) {
    int? money = 0;
    for (int i = 0; i < items3.length; i++) {
      print("${i + 1} => ${items3[i]}");
    }
    print('\nเลือกวิธีการชำระเงิน');
    var payment = stdin.readLineSync()!;
    while (money! > -1) {
      if (payment == '1') {
        print("จ่ายเงินสด");
        money = int.parse(stdin.readLineSync()!);
        if (money >= sum) {
          print("ชำระสำเร็จ");
          if (money > sum) {
            print("เงินทอน ${money - sum} บาท");
          }
          break;
        } else {
          print(
              "จำนวนเงินไม่เพียงพอ กรุณาชำระอีกครั้ง (ต้องการเปลี่ยนการชำระสินค้ากด '-1' )");
        }
      } else if (payment == '2') {
        print("จ่ายด้วย QR");
        money = int.parse(stdin.readLineSync()!);
        if (money >= sum) {
          print("ชำระสำเร็จ");
          break;
        } else {
          print(
              "จำนวนเงินในบัญชีของคุณไม่เพียงพอ กรุณาเติมเงินก่อนชำระอีกครั้ง (ต้องการเปลี่ยนการชำระสินค้ากด '-1' )");
        }
      } else if (payment == '3') {
        print("ช้อปปี้เพย์ (Shopee Pay)");
        money = int.parse(stdin.readLineSync()!);
        if (money >= sum) {
          print("ชำระสำเร็จ");
          break;
        } else {
          print(
              "จำนวนเงินใน Shopee Pay ของคุณไม่เพียงพอ กรุณาเติมเงินก่อนชำระอีกครั้ง (ต้องการเปลี่ยนการชำระสินค้ากด '-1' )");
        }
      } else if (payment == '4') {
        print("ใช้เต่าบินเครดิต");
        money = int.parse(stdin.readLineSync()!);
        if (money >= sum) {
          print("ชำระสำเร็จ");
          break;
        } else {
          print(
              "จำนวนแต้มเครดิตในเต่าบินเครดิตของคุณไม่เพียงพอ กรุณาชำระช่องทางอื่น (ต้องการเปลี่ยนการชำระสินค้ากด '-1' )");
        }
      } else if (payment == '5') {
        print("ใช้/ดู คูปองสะสม");
        money = int.parse(stdin.readLineSync()!);
        if (money >= sum) {
          print("ชำระสำเร็จ");
          break;
        } else {
          print(
              "จำนวนคูปองสะสมคุณไม่เพียงพอ กรุณาชำระช่องทางอื่น (ต้องการเปลี่ยนการชำระสินค้ากด '-1' )");
        }
      }
    }
    if (money != -1) break;
  }
  print('\nกรุณารอสักครู่ ระบบกำลังทำสินค้าของท่าน');
  for (int i = (5 * cart.length); i >= 0; i--) {
    sleep(Duration(seconds: 1));
    print('เหลือเวลา $i วินาที..');
  }
  print('ระบบดำเนินการเสร็จเรียบร้อย ขอบคุณที่ใช้บริการ Taobin ของเรา');
}
